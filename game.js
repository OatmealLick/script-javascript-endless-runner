let game;

let gameOptions = {
    platformStartSpeed: 100,
    playerSpeed: 200,
    spawnRange: [100, 350],
    platformSizeRange: [50, 250],
    playerGravity: 900,
    jumpForce: 400,
    playerStartPosition: 200,
    jumps: 2
}

window.onload = function() {

    let gameConfig = {
        type: Phaser.AUTO,
        width: 1334,
        height: 750,
        scene: playGame,
        backgroundColor: 0x444444,

        physics: {
            default: "arcade",
            arcade: {
                debug: true
            }
        }
    }
    game = new Phaser.Game(gameConfig);
    window.focus();
    resize();
    window.addEventListener("resize", resize, false);
}

class playGame extends Phaser.Scene{
    constructor(){
        super("PlayGame");
    }
    preload(){
        this.load.image("platform", "platform.png");
        this.load.image("player", "player.png");
        this.load.image("pickup", "pickup.png");
    }
    create(){

        this.points = 0;
        this.pickupSpawnTime = 3.0;
        this.pickupSpawnTimer = 0.0;
        this.platformGroup = this.add.group({
            removeCallback: function(platform){
                platform.scene.platformPool.add(platform)
            }
        });
        this.pickupGroup = this.add.group();

        this.platformPool = this.add.group({
            removeCallback: function(platform){
                platform.scene.platformGroup.add(platform)
            }
        });

        this.playerJumps = 0;
        this.addPlatform(game.config.width, game.config.width / 2);
        this.player = this.physics.add.sprite(gameOptions.playerStartPosition, game.config.height / 2, "player");
        this.player.setGravityY(gameOptions.playerGravity);
        this.physics.add.collider(this.player, this.platformGroup);

        this.pointsLabel = this.add.text(10, 10, 'Points: 0', { font: '30px Arial', fill: '#ffffff' });

        this.upKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
        this.leftKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
        this.rightKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);

        this.spawnPickup(500);
        this.spawnPickup(1000);
        this.timedEvent = this.time.addEvent({ delay: 5000, callback: this.spawnPickup, callbackScope: this, loop: true});
    }

    addPlatform(platformWidth, posX){
        let platform;
        if(this.platformPool.getLength()){
            platform = this.platformPool.getFirst();
            platform.x = posX;
            platform.active = true;
            platform.visible = true;
            this.platformPool.remove(platform);
        }
        else{
            platform = this.physics.add.sprite(posX, game.config.height * 0.8, "platform");
            platform.setImmovable(true);
            this.platformGroup.add(platform);
        }
        platform.displayWidth = platformWidth;
        this.nextPlatformDistance = Phaser.Math.Between(gameOptions.spawnRange[0], gameOptions.spawnRange[1]);
    }

    jump(){
        if(this.player.body.touching.down || (this.playerJumps > 0 && this.playerJumps < gameOptions.jumps)){
            if(this.player.body.touching.down){
                this.playerJumps = 0;
            }
            this.player.setVelocityY(gameOptions.jumpForce * -1);
            this.playerJumps ++;
        }
    }
    spawnPickup(positionX = 1400){
        let pickup = this.physics.add.sprite(positionX, 0.75 * game.config.height, "pickup");
        pickup.body.angularVelocity = 200;
        this.pickupGroup.add(pickup);
        this.physics.overlap(this.player, pickup, this.handlePlayerCollision, null, this);
        
    }

    handlePlayerCollision(playerSprite, pickupSprite){
        this.points += 10;
        this.pointsLabel.setText('Points: ' + this.points);
        pickupSprite.destroy();
    }
    update(){

        if(this.player.y > game.config.height){
            this.scene.start("PlayGame");
            this.points = 0;
        }

        let minDistance = game.config.width;
        this.platformGroup.getChildren().forEach(function(platform){
            let platformDistance = game.config.width - platform.x - platform.displayWidth / 2;
            minDistance = Math.min(minDistance, platformDistance);
            if(platform.x < - platform.displayWidth / 2){
                this.platformGroup.killAndHide(platform);
                this.platformGroup.remove(platform);
            }
        }, this);

        if(minDistance > this.nextPlatformDistance){
            var nextPlatformWidth = Phaser.Math.Between(gameOptions.platformSizeRange[0], gameOptions.platformSizeRange[1]);
            this.addPlatform(nextPlatformWidth, game.config.width + nextPlatformWidth / 2);
        }

        if (Phaser.Input.Keyboard.JustDown(this.upKey))
        {
            this.jump();
        }

        if (this.leftKey.isDown)
        {
            this.player.setVelocityX(gameOptions.playerSpeed * -1);
        }
        else if (this.rightKey.isDown)
        {
            this.player.setVelocityX(gameOptions.playerSpeed);
        }
        else 
        {
            this.player.setVelocityX(0);
        }

        const allowedDistance = 300;
        if (this.player.x > allowedDistance)
        {
            let offset = this.player.x - allowedDistance;
            this.player.x = allowedDistance;
            this.pickupGroup.getChildren().forEach(element => {
                element.x -= offset;
            });
            this.platformGroup.getChildren().forEach(element => {
                element.x -= offset;
            });
        }
        else if (this.player.x <= 5)
        {
            this.player.x = 5;
        }
    }
};
function resize(){
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = game.config.width / game.config.height;
    if(windowRatio < gameRatio){
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else{
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}
